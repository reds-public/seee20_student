
#!/bin/bash

# Deploy usr apps into the second partition
echo Deploying usr apps into the second partition...
cd ../filesystem
./mount.sh 2
sudo cp ../usr/out/* fs/root
./umount.sh

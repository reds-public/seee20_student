/*
 * Copyright (C) 2016 Daniel Rossier <daniel.rossier@soo.tech>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <linux/serial_reg.h>

.extern __uart_vaddr

		.macro	addruart, rp, rv, tmp
		ldr	\rp, =CONFIG_DEBUG_UART_PHYS
		ldr \rv, =__uart_vaddr
		ldr \rv, [\rv]T
		.endm

		.macro	senduart,rd,rx
		strb	\rd, [\rx, #UART_TX]
		.endm

		.macro	busyuart,rd,rx
		1002:
 		ldrb	\rd, [\rx, #UART_LSR*4]
 		and     \rd, \rd, #UART_LSR_TEMT | UART_LSR_THRE
        teq     \rd, #UART_LSR_TEMT | UART_LSR_THRE
		bne	1002b
		.endm

		.macro	waituart,rd,rx
		.endm




#ifndef MACH_UART_H
#define MACH_UART_H

#include <mach/rpi4.h>

#define CONFIG_DEBUG_UART_PHYS RPI4_UART0_PHYS
#define CONFIG_DEBUG_UART_VIRT RPI4_UART0_VIRT

#endif /* MACH_UART_H */


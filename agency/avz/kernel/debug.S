/*
 *  linux/arch/arm/kernel/debug.S
 *
 *  Copyright (C) 1994-1999 Russell King
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 *  32-bit debugging code
 */
		.text

/*
 * Some debugging routines (useful if you've got MM problems and
 * printk isn't working).  For DEBUGGING ONLY!!!  Do not leave
 * references to these in a production kernel!
 */

#include <mach/debug-macro.S>

.global printascii
.global printhex8
.global printch

.global __uart_vaddr

.macro	addruart_current, rx, tmp1, tmp2
		addruart	\tmp1, \tmp2
		mrc			p15, 0, \rx, c1, c0
		tst			\rx, #1
		moveq		\rx, \tmp1
		movne		\rx, \tmp2
.endm


/*
 * Useful debugging routines
 */
printhex8:
		mov	r1, #8
		b	printhex

printhex4:
		mov	r1, #4
		b	printhex

printhex2:
		mov	r1, #2
printhex:	adr	r2, hexbuf
		add	r3, r2, r1
		mov	r1, #0
		strb	r1, [r3]
1:		and	r1, r0, #15
		mov	r0, r0, lsr #4
		cmp	r1, #10
		addlt	r1, r1, #'0'
		addge	r1, r1, #'a' - 10
		strb	r1, [r3, #-1]!
		teq	r3, r2
		bne	1b
		mov	r0, r2
		b	printascii

		.ltorg

printascii:
		addruart_current r3, r1, r2
		b	2f
1:		waituart r2, r3
		senduart r1, r3
		busyuart r2, r3
		teq	r1, #'\n'
		moveq	r1, #'\r'
		beq	1b
2:		teq	r0, #0
		ldrneb	r1, [r0], #1
		teqne	r1, #0
		bne	1b
		mov	pc, lr

printch:
		addruart_current r3, r1, r2

		mov	r1, r0
		mov	r0, #0
		b	1b

hexbuf:		.space 16

__uart_vaddr:
		.word CONFIG_DEBUG_UART_VIRT


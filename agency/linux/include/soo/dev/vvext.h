/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef VVEXT_H
#define VVEXT_H

#include <linux/platform_device.h>

#include <soo/ring.h>
#include <soo/grant_table.h>

#include <soo/vbus.h>

#define VVEXT_PACKET_SIZE	32

#define VVEXT_NAME		"vvext"
#define VVEXT_PREFIX		"[" VVEXT_NAME "] "

typedef struct {
	char buffer[VVEXT_PACKET_SIZE];
} vvext_request_t;

typedef struct  {
	char buffer[VVEXT_PACKET_SIZE];
} vvext_response_t;

/*
 * Generate blkif ring structures and types.
 */
DEFINE_RING_TYPES(vvext, vvext_request_t, vvext_response_t);

typedef struct {
	vvext_back_ring_t ring;
	unsigned int irq;
	struct vbus_device *vdev;

} vvext_t;

void vvext_vbus_init(vvext_t *vvext, irq_handler_t irq_handler);

void vvext_probe(struct vbus_device *dev);
void vvext_close(struct vbus_device *dev);
void vvext_suspend(struct vbus_device *dev);
void vvext_resume(struct vbus_device *dev);
void vvext_connected(struct vbus_device *dev);
void vvext_reconfigured(struct vbus_device *dev);
void vvext_shutdown(struct vbus_device *dev);

bool vvext_start(domid_t domid);
void vvext_end(domid_t domid);
bool vvext_is_connected(domid_t domid);

#endif /* VVEXT_H */

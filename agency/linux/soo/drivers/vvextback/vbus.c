/*
 * Copyright (C) 2015-2018 Daniel Rossier <daniel.rossier@soo.tech>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <soo/evtchn.h>
#include <linux/slab.h>

#include <soo/gnttab.h>
#include <soo/hypervisor.h>
#include <soo/uapi/debug.h>

#include <soo/dev/vvext.h>

static vvext_t *__vvext = NULL;
static irq_handler_t vvext_interrupt;

bool __connected = false;

bool vvext_start(domid_t domid) {
	/* Actually do nothing at the moment... (seee) */
  return true;
}
EXPORT_SYMBOL(vvext_start);

void vvext_end(domid_t domid) {
	/* Actually do nothing at the moment... (seee) */
}
EXPORT_SYMBOL(vvext_end);

/*
 * Set up a ring (shared page & event channel) between the agency and the ME.
 */
static void setup_sring(struct vbus_device *dev) {
	int res;
	unsigned long ring_ref;
	unsigned int evtchn;
	vvext_t *vvext;
	vvext_sring_t *sring;

	vvext = (vvext_t *) dev_get_drvdata(&dev->dev);
	vvext->vdev = dev;

	vbus_gather(VBT_NIL, dev->otherend, "ring-ref", "%lu", &ring_ref, "ring-evtchn", "%u", &evtchn, NULL);

	DBG("BE: ring-ref=%u, event-channel=%u\n", ring_ref, evtchn);

	res = vbus_map_ring_valloc(dev, ring_ref, (void **) &sring);
	BUG_ON(res < 0);

	SHARED_RING_INIT(sring);
	BACK_RING_INIT(&vvext->ring, sring, PAGE_SIZE);

	SHARED_RING_INIT(sring);

	res = bind_interdomain_evtchn_to_virqhandler(dev->otherend_id, evtchn, vvext_interrupt, NULL, 0, VVEXT_NAME "-backend", vvext);

	BUG_ON(res < 0);

	vvext->irq = res;
}

/*
 * Free the ring and unbind evtchn.
 */
static void free_sring(struct vbus_device *dev) {
	vvext_t *vvext;

	vvext = (vvext_t *) dev_get_drvdata(&dev->dev);

	/* Prepare to empty all buffers */
	BACK_RING_INIT(&vvext->ring, (&vvext->ring)->sring, PAGE_SIZE);

	unbind_from_virqhandler(vvext->irq, vvext);

	vbus_unmap_ring_vfree(dev, vvext->ring.sring);
	vvext->ring.sring = NULL;
}

/*
 * Entry point to this code when a new device is created.
 */
static int __vvext_probe(struct vbus_device *dev, const struct vbus_device_id *id) {

	DBG("%s: SOO dummy driver for testing\n", __func__);

	dev_set_drvdata(&dev->dev, __vvext);

	return 0;
}

/*
 * Callback received when the frontend's state changes.
 */
static void frontend_changed(struct vbus_device *dev, enum vbus_state frontend_state) {

	DBG("%s\n", vbus_strstate(frontend_state));

	switch (frontend_state) {

	case VbusStateInitialised:
	case VbusStateReconfigured:
		DBG0("SOO Dummy: reconfigured...\n");

		setup_sring(dev);

		break;

	case VbusStateConnected:
		DBG0("vvext frontend connected, all right.\n");
		__connected = true;
		break;

	case VbusStateClosing:
		DBG0("Got that the virtual dummy frontend now closing...\n");

		free_sring(dev);
		__connected = false;

		break;

	case VbusStateSuspended:
		/* Suspend Step 3 */
		DBG("frontend_suspended: %s ...\n", dev->nodename);

		break;

	case VbusStateUnknown:
	default:
		break;
	}
}

static int __vvext_suspend(struct vbus_device *dev) {

	DBG0("vvext_suspend: wait for frontend now...\n");


	return 0;
}

static int __vvext_resume(struct vbus_device *dev) {
	/* Resume Step 1 */

	DBG("backend resuming: %s ...\n", dev->nodename);


	return 0;
}

bool vvext_is_connected(domid_t domid) {
	return __connected;
}
EXPORT_SYMBOL(vvext_is_connected);

/* ** Driver Registration ** */

static const struct vbus_device_id vvext_ids[] = {
	{ VVEXT_NAME },
	{ "" }
};

static struct vbus_driver vvext_drv = {
	.name = VVEXT_NAME,
	.owner = THIS_MODULE,
	.ids = vvext_ids,
	.probe = __vvext_probe,
	.otherend_changed = frontend_changed,
	.suspend = __vvext_suspend,
	.resume = __vvext_resume,
};

void vvext_vbus_init(vvext_t *vvext, irq_handler_t irq_handler) {

	__vvext = vvext;
	vvext_interrupt = irq_handler;

	vbus_register_backend(&vvext_drv);

}
EXPORT_SYMBOL(vvext_vbus_init);


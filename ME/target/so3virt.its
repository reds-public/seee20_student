/*
 * Copyright (C) 2018-2019 Daniel Rossier <daniel.rossier@soo.tech>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/dts-v1/;

/ {
	description = "SO3 image containing so3 kernel and FDT blob";
	#address-cells = <0>;
	#size-cells = <0>;
	
	images {
		#address-cells = <0>;
		#size-cells = <0>;
		kernel {
			/*reg = <>; Make dtc happy ;-) */
			description = "SO3 OS kernel";
			data = /incbin/("../so3/so3.bin");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "none";
			
			#address-cells = <0>;
			#size-cells = <0>;
			
			hash {
				algo = "crc32";
			};
		};
		fdt {
			description = "Flattened Device Tree blob";
			data = /incbin/("../so3/arch/arm/boot/dts/so3virt.dtb");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
			hash {
				algo = "crc32";
			};
		};	

                ramdisk {
                        description = "SO3 environment minimal rootfs";
                        data = /incbin/("../rootfs/board/so3virt/rootfs.fat");
                        type = "ramdisk";
                        arch = "arm";
                        os = "linux";
                        compression = "none";
                };

    };
	configurations {
		default = "conf";
		conf {
			description = "SO3 kernel image including device tree";
			kernel = "kernel";
			fdt = "fdt";
                        ramdisk = "ramdisk";
		};
	};
};

#!/bin/bash

echo Deploying all MEs into the third partition...


cd ../agency/filesystem

./mount.sh 3

sudo rm -rf fs/*

if [ "$1" != "clean" ]; then
        cd ../../ME/target
        ./mkuboot.sh so3virt
        cd ../../agency/filesystem
        ME_to_deploy="../../ME/target/so3virt.itb"
        sudo cp -rf $ME_to_deploy fs/
        echo "$1 deployed"
fi
./umount.sh

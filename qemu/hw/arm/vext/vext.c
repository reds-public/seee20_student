/*
 * Copyright (C) 2013-2014 Romain Bornet <romain.bornet@heig-vd.ch>
 * Copyright (C) 2016-2020 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "vext.h"
#include "vext_emul.h"

static vext_state_t *vext_state;

void vext_process_switch(cJSON *packet)
{
	int i, switch_nr = 0;

	cJSON *obj = cJSON_GetObjectItem(packet, "status");

	if (obj)
	{
		uint16_t switch_reg = (uint16_t) obj->valueint;

		DBG("%s Updating switch: %d -> 0x%x \n", __func__, VEXT_SWITCH, switch_reg);

		if ((vext_state->regs[VEXT_IRQ_CTRL] & VEXT_IRQ_STATUS_MASK) == 0) {

			if (switch_reg) {
				vext_state->regs[VEXT_SWITCH] = switch_reg;
				for (i = 0; i < 8; i++)
					if (switch_reg & (1 << i)) {
						switch_nr = i;
						break;
					}
			}
			if (vext_state->regs[VEXT_IRQ_CTRL] & VEXT_IRQ_EN_MASK) {
				vext_state->regs[VEXT_IRQ_CTRL] |= ((switch_nr << 1) | VEXT_IRQ_STATUS_MASK);

				DBG("%s IRQ rise. Switch nr: %d\n", __func__, switch_nr+1);

				qemu_irq_raise(vext_state->irq);
			}
		}
	}
}

static uint64_t vext_read(void *opaque, hwaddr addr, unsigned size)
{
	vext_state_t *s = (vext_state_t *) opaque;

	/* Be sure that the read is valid */
	if ((addr != VEXT_SWITCH) && (addr != VEXT_IRQ_CTRL))
		return 0;

	return s->regs[addr];
}

static int irq_enabled(unsigned int val) {
	return ((val & VEXT_IRQ_EN_MASK) ? 1 : 0);
}

static int irq_pending(unsigned int val) {
	return ((val & VEXT_IRQ_STATUS_MASK) ? 1 : 0);
}

static void vext_write(void *opaque, hwaddr addr, uint64_t value, unsigned size)
{
	vext_state_t *s = (vext_state_t *) opaque;

	DBG("%s vext_write at address offset 0x%x value: 0x%x\n", __func__, (unsigned int) addr, (unsigned int) value);

	cJSON *packet = cJSON_CreateObject();

	switch (addr) {

	case VEXT_LED:
		DBG("%s VEXT_LED\n", __FUNCTION__);
		cJSON_AddStringToObject(packet, "device", DEV_LED);
		cJSON_AddNumberToObject(packet, "value", (unsigned int) value);

		vext_cmd_post(packet);
		break;

	case VEXT_IRQ_CTRL:
		DBG("%s VEXT_IRQ_CTL\n", __FUNCTION__);
		DBG("%s VEXT_IRQ_CTL en|pending %d|%d\n", __func__, irq_enabled(s->regs[VEXT_IRQ_CTRL]), irq_pending(s->regs[VEXT_IRQ_CTRL]));

		if (value & VEXT_IRQ_CLEAR_MASK)
		{
			DBG("%s VEXT_IRQ_CTL: clearing IRQ\n", __func__);

			value = s->regs[VEXT_IRQ_CTRL] & ~(VEXT_IRQ_STATUS_MASK | VEXT_IRQ_CLEAR_MASK);

			qemu_irq_lower(s->irq);
		}
		DBG("%s VEXT_IRQ_CTL en|pending %d|%d\n", __func__, irq_enabled(value), irq_pending(value));

		break;
	}

	s->regs[addr] = (uint16_t) value;

}

static const MemoryRegionOps vext_ops = {
	.read = vext_read,
	.write = vext_write,
	.endianness = DEVICE_NATIVE_ENDIAN,
};

static void vext_init(Object *obj)
{
	DeviceState *dev = DEVICE(obj);
	SysBusDevice *sbd = SYS_BUS_DEVICE(obj);
	vext_state_t *s = OBJECT_CHECK(vext_state_t, dev, "vext");

	memory_region_init_io(&s->iomem, OBJECT(s), &vext_ops, s, "vext", 0x1000);
	sysbus_init_mmio(sbd, &s->iomem);

	sysbus_init_irq(sbd, &s->irq);

	vext_emul_init();

	vext_state = s;

	// vext_clcd_init();
}

static const TypeInfo vext_info = {
	.name          = "vext",
	.parent        = TYPE_SYS_BUS_DEVICE,
	.instance_size = sizeof(vext_state_t),
	.instance_init    = vext_init,
};

static void vext_register_types(void)
{
	type_register_static(&vext_info);
}

type_init(vext_register_types)

#!/bin/bash
 
echo Installing new SenseHat GUI frontend and server ...
 
sudo apt-get -y install python-pip
pip install --upgrade pip
cd sense-hat
python -m pip install sense_emu-1.1-py2-none-any.whl
 
